<?php

$students = ['John', 'Steve', 'Julia'];


// echo $students;
print_r($students);

echo '<br>';

$name = $students[1];

echo $name . ' and ' . $students[2];

$students[1] = 'Bob';

echo '<br>';
print_r($students);

$students[45] = 'Alex';
$students[36] = 'John';

echo '<br>';
print_r($students);

$student = [
	'email' => 'john@email.com',
	'phone' => '+380968789374', 
	'name' => 'John', 
	'age' => 45,
	4 => 45345
];

echo '<br>';
print_r($student);


echo 'Name: ' . $student['name'];

$student['weight'] = 80;

echo '<br>';
print_r($student);

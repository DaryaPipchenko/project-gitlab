<?php
$student = [
	'email' => 'john@email.com',
	'phone' => '+380968789374', 
	'name' => 'John', 
	'age' => 45,
];

?>
<!DOCTYPE html>
<html>
<head>
	<title>Page</title>
</head>
<body>
	<h1>Student Data</h1>
	<p>Name: <?= $student['name'] ?></p>
	<p>Email: <?= $student['email'] ?></p>
	<p>Phone: <?php echo $student['phone'] ?></p>
	<p>Age: <?php echo $student['age'] ?></p>
</body>
</html>
<?php

// echo 'Hello world';

// This is comment

# This is comment

/*
this
is
comment
*/

$firstName;
$_some_var12;
// $12wrong; // incorrect

$name = 'Steve';

$age = 34;

echo "\$Hello, \"$name\". Your age: $age";

$a = 3;
$a = 4;

$b = $a;

$checked = false; //boolean
$temperature = 37.1; // float

$empty = null;

$a = '25';

$b = 45;

$c = $a + $b;

echo $c;

$d = 'Lesson';
$number = 3;

echo '<br>';

echo $d . $number;

// echo $d + $number;

$a = (int) $d;

echo '<br>';

echo $a;

$a = 434;
$b = (bool) $a;

var_dump($b);

echo $b;
